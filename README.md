## Inicio rápido

`chmod 600 acme.json`

`docker network create proxy`

`docker-compose up`

Navegar a http://localhost:8080

Configurar datos de certificados TLS/HTTPS en `traefik.yml`

## Con Docker run
```
docker run --name traefik-test -p 8080:8080 -p 80:80 \
-v $PWD/traefik.yml:/etc/traefik/traefik.yml \
-v /var/run/docker.sock:/var/run/docker.sock \
traefik:v2.4
```

## Probar rutas
Pegándole al traefik desde adentro y spoofeando el host:

`curl --header 'Host:subdominio.mooo.com' http://localhost:80/`

O pegando desde afuera usando a tor de proxy:

`torify curl -v http://subdominio.mooo.com/`

## Levantar nginx local de prueba
Para esto pueden pedir un subdominio gratis en https://freedns.afraid.org/subdomain/
y apuntarlo a su IP. Después forwardear los puertos 80 y 443 de su rúter a su PC.
Configurar el subdominio en las labels de compose del nginx. Levantar todo y reiniciar
servicios si hace falta y listo, tu dominio tendrá certificado TLS de LetsEncrypt hecho.

`docker-compose -f docker-compose-nginx.yml up`

## Refes
https://medium.com/@containeroo/traefik-2-0-docker-a-simple-step-by-step-guide-e0be0c17cfa5

https://dev.to/cedrichopf/get-started-with-traefik-2-using-docker-compose-35f9

https://www.apoehlmann.com/blog/setting-up-traefik-with-docker-composer/

Oficiales:

https://hub.docker.com/_/traefik/

https://doc.traefik.io/traefik/providers/docker/

https://doc.traefik.io/traefik/getting-started/configuration-overview/
